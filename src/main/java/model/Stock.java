/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Stock {
    private int stockid;
    private String name;
    private int amount;
    
    public Stock(){
    }
    
    public Stock(int stock_id , String name , int amouunt){
        this.stockid = stock_id;
        this.name = name;
        this.amount = amouunt;
    }

    public int getStockid() {
        return stockid;
    }

    public void setStockid(int stockid) {
        this.stockid = stockid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    @Override
    public String toString(){
        return "Id : " + stockid + " Name : " + name + " Amount : " + amount;
    }
}