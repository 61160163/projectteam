/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author user
 */
public class OrderDetail {
    private int orderD_Id;
    private int order_Id;
    private int product_Id;
    private Product product;
    private Order order;
    private int orderD_Amount;
    private double orderD_Price;
    private String productName;

    public OrderDetail(int orderD_Id, int order_Id, int product_Id, int orderD_Amount, double orderD_Price) {
        this.orderD_Id = orderD_Id;
        this.order_Id = order_Id;
        this.product_Id = product_Id;
        this.orderD_Amount = orderD_Amount;
        this.orderD_Price = orderD_Price;
    }
public OrderDetail(int orderD_Id,Order order, Product product, int orderD_Amount, double orderD_Price){
    this.orderD_Id = orderD_Id;
        this.order = order;
        this.product = product;
        this.orderD_Amount = orderD_Amount;
        this.orderD_Price = orderD_Price;
}

    public OrderDetail(int order_Id, String product_name, int orderD_Amount, double orderD_Price) {
       this.order_Id = order_Id;
       this.productName = product_name;
       this.orderD_Amount = orderD_Amount;
       this.orderD_Price = orderD_Price;
    }

    public OrderDetail(int order_Id, int product_Id, int orderD_Amount, double orderD_Price) {
        this.order_Id = order_Id;
        this.product_Id = product_Id;
        this.orderD_Amount = orderD_Amount;
        this.orderD_Price = orderD_Price;
    }
    
    

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    public int getOrderD_Id() {
        return orderD_Id;
    }

    public void setOrderD_Id(int orderD_Id) {
        this.orderD_Id = orderD_Id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getOrder_Id() {
        return order_Id;
    }

    public void setOrder_Id(int order_Id) {
        this.order_Id = order_Id;
    }

    public int getProduct_Id() {
        return product_Id;
    }

    public void setProduct_Id(int product_Id) {
        this.product_Id = product_Id;
    }

    public int getOrderD_Amount() {
        return orderD_Amount;
    }

    public void setOrderD_Amount(int orderD_Amount) {
        this.orderD_Amount = orderD_Amount;
    }

    public double getOrderD_Price() {
        return orderD_Price;
    }

    public void setOrderD_Price(double orderD_Price) {
        this.orderD_Price = orderD_Price;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "orderD_Id=" + orderD_Id + ", order_Id=" + order_Id + ", product_Id=" + product_Id + ", orderD_Amount=" + orderD_Amount + ", orderD_Price=" + orderD_Price + '}';
    }
}
