/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author user
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int id = -1;
        try {
            String sql = "INSERT INTO product ( product_name, product_price )VALUES (?, ?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT product_id,product_name,product_price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                Product product = new Product(id,name,price);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT product_id,product_name,product_price FROM product WHERE product_id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
                int pid = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                Product product = new Product(id,name,price);
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE product_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "UPDATE product SET product_name = ?, product_price = ? WHERE product_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
        return row;
    }
    public ArrayList<Product> getAllImages(){
         ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT product_id,product_name,product_price,product_image FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                String images = result.getString("product_image");
                Product product = new Product(id,name,price,images);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
        return list;
    }
    
    public ArrayList<Product> searchProduct(String string) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM product WHERE product_name LIKE"+"\'"+string+"%"+"\'"+"OR product_id Like"+"\'"+string+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");  
                String image = result.getString("product_image");
                Product product = new Product(id,name,price,image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error: search"+ex.getMessage());
        }
        db.close();
        return list;
    }
}
