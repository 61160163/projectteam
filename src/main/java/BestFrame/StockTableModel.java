/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BestFrame;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Customer;
import model.Stock;

/**
 *
 * @author User
 */
public class StockTableModel extends AbstractTableModel{
    private Stock stk;
    private ArrayList<Stock> stock;
    private String[] stockDe = {"stock_id" , "stock_name" , "stock_amount"};
    
    public StockTableModel(ArrayList<Stock> listData){
        stock = listData;
    }
    
    public StockTableModel(Stock stock) {
        this.stk = stock;
    }

    @Override
    public int getRowCount() {
        return this.stock.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Stock stk = this.stock.get(rowIndex);
        if (columnIndex == 0) {
            return stk.getStockid();
        }
        
        if (columnIndex == 1) {
            return stk.getName();
        }
        
        if (columnIndex == 2) {
            return stk.getAmount();
        }
        
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return stockDe[column];
    }
    
    
    
}
